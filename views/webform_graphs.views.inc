<?php

/**
 * @author Ruchira Wageesha <ruchira.wageesha@gmail.com>
 */

function webform_graphs_views_data_alter(&$data) {

    $data['webform_submissions']['webform_graph'] = array(
        'field' => array(
            'title' => t('Graphs'),
            'help' => t('Provide Graphs for Webform Submissions'),
            'handler' => 'webform_graphs_handler_field_graph',
        ),
    );
    $data['webform_submissions']['nid']['filter'] = array(
        'title' => t('Nid'),
        'handler' => 'views_handler_filter_numeric',
        'help' => t('Nid of the Webform'),
    );
    $data['webform_submissions']['uid']['filter'] = array(
        'title' => t('User Id'),
        'handler' => 'views_handler_filter_numeric',
        'help' => t('User Id of the submission'),
    );
    $data['webform_submissions']['nid']['argument'] = array(
        'title' => t('Webform Nid'),
        'help' => t('Nid of the Webform'),
        'handler' => 'views_handler_argument_numeric',
        'name field' => 'title', // the field to display in the summary.
        'numeric' => TRUE,
        'validate type' => 'nid',
    );
    $data['webform_submissions']['submission_data'] = array(
        'field' => array(
            'title' => t('Submission Data'),
            'help' => t('Provide Submission Data of Webform Submissions'),
            'handler' => 'webform_graphs_handler_field_submission_data',
        ),
    );
    $data['webform_submissions']['submission_info'] = array(
        'field' => array(
            'title' => t('Submission Info'),
            'help' => t('Provide Submission Info of Webform Submissions'),
            'handler' => 'webform_graphs_handler_field_submission_info',
        ),
    );
    $data['webform_submissions']['sid']['argument'] = array(
        'title' => t('Submission Id'),
        'help' => t('Submission id of the Webform'),
        'handler' => 'views_handler_argument_numeric',
        'name field' => 'title', // the field to display in the summary.
        'numeric' => TRUE,
        'validate type' => 'sid',
    );
    $data['webform_submissions']['uid']['argument'] = array(
        'title' => t('User Id'),
        'help' => t('User Id of the submission'),
        'handler' => 'views_handler_argument_numeric',
        'name field' => 'title', // the field to display in the summary.
        'numeric' => TRUE,
        'validate type' => 'uid',
    );
    return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function webform_graphs_views_handlers() {
    return array(
        'info' => array(
            'path' => drupal_get_path('module', 'webform_graphs') . '/views',
        ),
        'handlers' => array(
            'webform_graphs_handler_field_graph' => array(
                'parent' => 'views_handler_field',
                'file' => 'webform_graphs_handler_field_graph.inc',
            ),
            'webform_graphs_handler_field_submission_data' => array(
                'parent' => 'views_handler_field',
                'file' => 'webform_graphs_handler_field_submission_data.inc',
            ),
            'webform_graphs_handler_field_submission_info' => array(
                'parent' => 'views_handler_field',
                'file' => 'webform_graphs_handler_field_submission_info.inc',
            ),
        ),
    );
}