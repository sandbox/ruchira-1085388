<?php

/**
 * @author Ruchira Wageesha <ruchira.wageesha@gmail.com>
 */

/**
 * Field handler to present a link to the user.
 */
class webform_graphs_handler_field_graph extends views_handler_field {
    var $link_type;

    function construct() {
        // We need to set this property before calling the construct() chain
        // as we use it in the option_definintion() call.
        parent::construct();
        $this->additional_fields['nid'] = array('table' => 'webform_submissions', 'field' => 'nid');
        $this->additional_fields['sid'] = array('table' => 'webform_submissions', 'field' => 'sid');
    }

    function allow_advanced_render() {
        return FALSE;
    }

    function option_definition() {
        $options = parent::option_definition();
        $options['graph-' . '1'] = array('default' => '');
        $webform_types = webform_variable_get('webform_node_types');
        $nodes = array();
        if ($webform_types) {
            $nodes = db_select('node', 'n')
                    ->fields('n', array('nid', 'title'))
                    ->condition('n.type', $webform_types, 'IN')
                    ->execute()
                    ->fetchAllAssoc('nid');
            foreach ($nodes as $nid => $data) {
                $options['graph-' . $nid] = array('default' => '');
            }
        }
        return $options;
    }

    function options_form(&$form, &$form_state) {
        parent::options_form($form, $form_state);
        $webform_types = webform_variable_get('webform_node_types');
        $nodes = array();
        if ($webform_types) {
            $nodes = db_select('node', 'n')
                    ->fields('n', array('nid', 'title'))
                    ->condition('n.type', $webform_types, 'IN')
                    ->execute()
                    ->fetchAllAssoc('nid');
            foreach ($nodes as $nid => $data) {
                $graphs = webform_graphs_graph_load(NULL, $nid);
                $options = array('0' => '--' . t('None') . '--');
                foreach ($graphs['data'] as $graph) {
                    $options[$graph['gid']] = t($graph['name']);
                }
                $form['graph-' . $nid] = array(
                    '#type' => 'select',
                    '#title' => t($data->title),
                    '#options' => $options,
                    '#default_value' => !empty($this->options['graph-' . $nid]) ? $this->options['graph-' . $nid] : '',
                );
            }
        }
    }

    function query() {
        $this->ensure_my_table();
        // Join to the node table to retrieve the node UID.
        $this->add_additional_fields();
    }

    function render($values) {
        $nid = $values->{$this->aliases['nid']};
        $gid = $this->options['graph-' . $nid];
        //return $gid;
        if ($gid != 0) {
            module_load_include('inc', 'webform', 'includes/webform.submissions');
            module_load_include('inc', 'webform_graphs', 'includes/webform_graphs.graph');
            $node = node_load($nid);
            $graph = webform_graphs_graph_load($gid, $nid);
            if (count($graph['data']) > 0) {
                $graph = $graph['data'][0];
                //$webform variable is used within the eval() call below
                $webform = webform_graphs_prepare_data_array($node, webform_get_submission($node->nid, $values->{$this->aliases['sid']}, TRUE));
                $title = trim($graph['title']);
                $title = (!empty($title) or $title == NULL) ? '<h4>' . t($title) . '</h4>' : '';
                return $title . theme('graphael', eval($graph['config']));
            }
        }
    }
}