<?php

/**
 * @author Ruchira Wageesha <ruchira.wageesha@gmail.com>
 */

/**
 * Field handler to present a link to the user.
 */
class webform_graphs_handler_field_submission_data extends views_handler_field {

    function construct() {
        // We need to set this property before calling the construct() chain
        // as we use it in the option_definintion() call.
        parent::construct();
        $this->additional_fields['nid'] = array('table' => 'webform_submissions', 'field' => 'nid');
        $this->additional_fields['sid'] = array('table' => 'webform_submissions', 'field' => 'sid');
    }

    function allow_advanced_render() {
        return FALSE;
    }

    function query() {
        $this->ensure_my_table();
        // Join to the node table to retrieve the node UID.
        $this->add_additional_fields();
    }

    function render($values) {
        $nid = $values->{$this->aliases['nid']};
        module_load_include('inc', 'webform', 'includes/webform.submissions');
        return webform_submission_render(node_load($nid), webform_get_submission($nid, $values->{$this->aliases['sid']}, TRUE), NULL, 'html');
    }
}
