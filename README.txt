Webform Graphs Module 
=====================

This module was developed with the intention of creating graphs for webform results. Further you can define as
many charts you want for a particular webform and get them as charts in view fields.When you installed this
webform_graphs module, you will get the following list of features.

1. Define any number of charts for each Webform and get them drawn with submitted data as views's fields
2. Webform views fields for Submission Data and Submission Info
3. Webform views arguments for Submission Id, Submission Uid and Webform Nid
4. Webform views filter for Submission Id, Submission Uid and Webform Nid


Usage Instructions
==================

1. Install the module as you install any other Drupal module. This will create a new table called "webfrom_graphs"
   in your database.
2. Go to the webform where you want to add graphing support and find the "Graph" tab under the "Webform" tab.
3. There you can give an administrative title for the graph and click on the "Add" button.
4. You will prompted a new form for further configuration. Here you need to add php based raphael graph configuration.
   There you can do whatever processing you need with webform submission data and return an array as in Reference-1
   below. All the data of the currently viewing submission is available in the array named $webform. Structure of
   the array is at the Reference-2 below.
5. Go to views and create a new view with the view type "Webform submissions".
6. Then you can find the "Graphs" field under fields. When you are adding that field, you will get a select element for
   each webform with their graphs we added at step #4 above.
7. You can optionally use following data within your views
        Fields      =>  Submission Data, Submission Info
        Arguments   =>  Submission Id, Submission Uid, Webform Nid
        Filters     =>  Submission Id, Submission Uid, Webform Nid
8. So you can now use you graphs wherever you want. e.g. panels, blocks etc.



Reference-1
===========

Following is a sample code fragment that you can put as the raphael configuration at #4 above. If you want to know more
about available graphing properties, please have a look at raphael/graphael/graphael_example/graphael_example.graphs.inc
which comes with raphael module. You need to return an array(foo) in the same format as in the call for
theme('raphael', foo);

Note : You don't need to add <?php tag here and please don't put any invalid php code fragments.

$popdata = array(
    // Africa, Asia, Europe, Latin America, North America, Oceania
    '1750' => array(106, 502, 163, 16, 2, 2),
    '1800' => array(107, 635, 203, 24, 7, 2),
    '1850' => array(111, 809, 276, 38, 26, 2),
    '1900' => array(133, 947, 408, 74, 82, 6),
    '1950' => array(211, 1398, 547, 167, 172, 12.8),
    '2000' => array(796, 3680, 728, 520, 316, 31),
);

return array(
    'method' => 'bar',
    'values' => $popdata[2000],
    'params' => array(
        'colors' => array('#490A3D', '#BD1550', '#E97F02', '#F8CA00', '#8A9B0F', '#68B3AF'),
        'font' => '10px Arial, sans-serif',
        'opts' => array(
            'gutter' => '80%',
        ),
        'label' => array(
            'values' => array('Africa', 'Asia', 'Europe', 'L. America', 'N. America', 'Oceania'),
            'isBottom' => TRUE,
        ),
    ),
    'extend' => array(
        'label' => array(
            'values' => array('Africa', 'Asia', 'Europe', 'Latin America', 'Northern America', 'Oceania'),
            'params' => array('attrText' => array(
                'fill' => '#aaa',
                'font' => '10px Arial, sans-serif',
            )),
        ),
    ),
);



Reference-2
===========

Following is the structure of the $webform array which can be used to retrieve submission data in your code graph
configuration code. You can view this array anytime by calling devel module's dsm(print_r($webform, TRUE));
method in your graph configuration and accessing a view.

Further all data have been keyed with relavant field names in of webform. i.e. If we have a webform with
Username and Email field which have machine names as 'fusername' and 'femail', then the $webform array will be in the form
of

array(
    'fusername' => array(....),
    'fname' => array(....),
);

Here is a sample array of the $webform which I got using with a form with all form components. As webform allows us to add
many fields with the same name, they will be added as a list of arrays referred by the key.


Array
(
    [fdate] => Array
        (
            [nid] => 1
            [cid] => 1
            [pid] => 0
            [form_key] => fdate
            [name] => fDate
            [type] => date
            [value] => Array
                (
                    [0] => 2011-02-04
                )

            [extra] => Array
                (
                    [timezone] => user
                    [title_display] => 0
                    [datepicker] => 1
                    [year_textfield] => 0
                    [year_start] => -2
                    [year_end] => +2
                    [conditional_operator] => =
                    [description] =>
                    [conditional_component] =>
                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 0
            [page_num] => 1
        )

    [femail] => Array
        (
            [nid] => 1
            [cid] => 2
            [pid] => 3
            [form_key] => femail
            [name] => fEmail
            [type] => email
            [value] => Array
                (
                    [0] => ruchirawageesha@gmail.com
                )

            [extra] => Array
                (
                    [title_display] => 0
                    [disabled] => 0
                    [unique] => 0
                    [conditional_operator] => =
                    [width] =>
                    [description] =>
                    [attributes] => Array
                        (
                        )

                    [conditional_component] =>
                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 1
            [page_num] => 1
        )

    [ffile] => Array
        (
            [nid] => 1
            [cid] => 4
            [pid] => 3
            [form_key] => ffile
            [name] => fFile
            [type] => file
            [value] => Array
                (
                    [0] => 1
                )

            [extra] => Array
                (
                    [title_display] => 0
                    [filtering] => Array
                        (
                            [types] => Array
                                (
                                    [0] => gif
                                    [1] => jpg
                                    [2] => png
                                )

                            [addextensions] =>
                            [size] => 800
                        )

                    [conditional_operator] => =
                    [savelocation] =>
                    [width] =>
                    [description] =>
                    [attributes] => Array
                        (
                        )

                    [conditional_component] =>
                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 2
            [page_num] => 1
        )

    [fgrid] => Array
        (
            [nid] => 1
            [cid] => 5
            [pid] => 0
            [form_key] => fgrid
            [name] => fGrid
            [type] => grid
            [value] => Array
                (
                    [1] => 1
                    [2] => 2
                    [3] => 3
                )

            [extra] => Array
                (
                    [options] => 1|Name
2|Age
3|Birth
                    [questions] => 1|What is your name
2|What is your age
3|What is your birthday
                    [title_display] => 0
                    [optrand] => 0
                    [qrand] => 0
                    [conditional_operator] => =
                    [custom_option_keys] => 0
                    [custom_question_keys] => 0
                    [description] =>
                    [conditional_component] =>
                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 3
            [page_num] => 1
        )

    [fhidden] => Array
        (
            [nid] => 1
            [cid] => 6
            [pid] => 0
            [form_key] => fhidden
            [name] => fHidden
            [type] => hidden
            [value] => Array
                (
                    [0] => fvalue
                )

            [extra] => Array
                (
                    [conditional_operator] => =
                    [conditional_component] =>
                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 4
            [page_num] => 1
        )

    [fselectoptions] => Array
        (
            [nid] => 1
            [cid] => 9
            [pid] => 0
            [form_key] => fselectoptions
            [name] => fSelectOptions
            [type] => select
            [value] => Array
                (
                    [0] => 1
                )

            [extra] => Array
                (
                    [items] => 1|Name
2|Age
4|Birth
                    [multiple] => 0
                    [title_display] => 0
                    [aslist] => 0
                    [optrand] => 0
                    [conditional_component] => 2
                    [conditional_operator] => =
                    [other_option] =>
                    [other_text] => Other...
                    [description] =>
                    [custom_keys] =>
                    [options_source] =>
                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 7
            [page_num] => 2
        )

    [ftextarea] => Array
        (
            [nid] => 1
            [cid] => 10
            [pid] => 3
            [form_key] => ftextarea
            [name] => fTextarea
            [type] => textarea
            [value] => Array
                (
                    [0] => fTextarea Content
                )

            [extra] => Array
                (
                    [title_display] => 0
                    [resizable] => 1
                    [disabled] => 0
                    [conditional_component] => 2
                    [conditional_operator] => =
                    [cols] =>
                    [rows] =>
                    [description] =>
                    [attributes] => Array
                        (
                        )

                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 3
            [page_num] => 1
        )

    [ftextfield] => Array
        (
            [nid] => 1
            [cid] => 11
            [pid] => 0
            [form_key] => ftextfield
            [name] => fTextfield
            [type] => textfield
            [value] => Array
                (
                    [0] => fTextfield Content
                )

            [extra] => Array
                (
                    [title_display] => 0
                    [disabled] => 0
                    [unique] => 0
                    [conditional_component] => 2
                    [conditional_operator] => =
                    [width] =>
                    [maxlength] =>
                    [field_prefix] =>
                    [field_suffix] =>
                    [description] =>
                    [attributes] => Array
                        (
                        )

                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 9
            [page_num] => 2
        )

    [ftime] => Array
        (
            [nid] => 1
            [cid] => 12
            [pid] => 0
            [form_key] => ftime
            [name] => fTime
            [type] => time
            [value] => Array
                (
                    [0] => 10:10:00
                )

            [extra] => Array
                (
                    [timezone] => user
                    [title_display] => 0
                    [hourformat] => 12-hour
                    [conditional_component] => 2
                    [conditional_operator] => =
                    [description] =>
                    [conditional_values] =>
                )

            [mandatory] => 0
            [weight] => 10
            [page_num] => 2
        )

)

