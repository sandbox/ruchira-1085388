<?php

/**
 * @author Ruchira Wageesha <ruchira.wageesha@gmail.com>
 */

function webform_graphs_graph_form($form, $form_state, $node) {

    $path = drupal_get_path('module', 'webform');
    $form = array(
        '#tree' => TRUE,
        '#node' => $node,
        '#attached' => array(
            'css' => array($path . '/css/webform-admin.css' => array('preprocess' => FALSE, 'weight' => CSS_DEFAULT + 1)),
            'js' => array($path . '/js/webform-admin.js' => array('preprocess' => FALSE)),
        ),
        'components' => array(),
    );

    $form['nid'] = array(
        '#type' => 'value',
        '#value' => $node->nid,
    );

    $graphs = webform_graphs_graph_load(NULL, $node->nid);
    foreach ($graphs['data'] as $graph) {
        $form['graphs'][$graph['gid']]['name'] = array(
            '#markup' => check_plain($graph['name']),
        );
    }

    $form['add'] = array(
        '#theme' => 'webform_graphs_graph_add_form',
        '#tree' => FALSE,
    );

    $form['add']['graph_name'] = array(
        '#title' => t("Administrative title of the Graph"),
        '#type' => 'textfield',
        '#size' => 24,
        '#maxlength' => 500,
    );

    $form['add_button'] = array(
        '#type' => 'submit',
        '#value' => t('Add'),
        '#weight' => 45,
    );

    $form['#validate'] = array('webform_graphs_graph_name_validate');

    return $form;
}

/**
 * Theme the node components form. Use a table to organize the components.
 *
 * @param $form
 *   The form array.
 * @return
 *   Formatted HTML form, ready for display.
 */
function theme_webform_graphs_graph_form($variables) {
    $form = $variables['form'];
    $node = $form['#node'];

    $header = array(array('data' => t('Graph Name'), 'colspan' => 3), array('data' => t('Operations'), 'colspan' => 2));
    $rows = array();

    if (!empty($form['graphs'])) {
        foreach (element_children($form['graphs']) as $gid) {
            // Add each component to a table row.
            $rows[] = array(
                array('data' => drupal_render($form['graphs'][$gid]['name']), 'colspan' => 3),
                l(t('Edit'), 'node/' . $node->nid . '/webform/graphs/' . $gid),
                l(t('Delete'), 'node/' . $node->nid . '/webform/graphs/' . $gid . '/delete'),
            );
        }
    }
    else {
        $rows[] = array(array('data' => t('Currently no Graphs have been added.'), 'colspan' => 5));
    }

    // Add a row containing form elements for a new item.
    $row_data = array(
        array('colspan' => 3, 'data' => drupal_render($form['add'])),
        array('colspan' => 2, 'data' => drupal_render($form['add_button'])),
    );
    $rows[] = array('data' => $row_data, 'class' => array('webform-add-form'));

    $output = '';
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'webform-emails')));
    $output .= drupal_render_children($form);
    return $output;
}

/**
 * Theme the add new e-mail settings form on the node/x/webform/emails page.
 */
function theme_webform_graphs_graph_add_form($variables) {
    $form = $variables['form'];
    // Add a default value to the custom e-mail textfield.
    $form['graph_name']['#attributes']['rel'] = t('Enter you Graph name');
    $form['graph_name']['#attributes']['class'] = array('webform-set-active', 'webform-default-value');
    return drupal_render_children($form);
}

/**
 * Submit handler for webform_emails_form().
 */
function webform_graphs_graph_form_submit($form, &$form_state) {
    $form_state['redirect'] = array(
        'node/' . $form['#node']->nid . '/webform/graphs/new',
        array(
            'query' => array(
                'name' => trim($form_state['values']['graph_name']),
            )
        )
    );
}

/**
 * Form for configuring an e-mail setting and template.
 */
function webform_graphs_graph_edit_form($form, $form_state, $node, $graphs = array()) {
    $path = drupal_get_path('module', 'webform');
    $form = array(
        '#tree' => TRUE,
        '#attached' => array(
            'css' => array($path . '/css/webform-admin.css' => array('preprocess' => FALSE, 'weight' => CSS_DEFAULT + 1)),
            'js' => array(
                $path . '/js/webform-admin.js' => array('preprocess' => FALSE),
                array('data' => array('webform' => array('revertConfirm' => t('Are you sure you want to revert any changes to your Graph to the default?'))), 'type' => 'setting'),
            ),
        ),
    );
    $form['node'] = array(
        '#type' => 'value',
        '#value' => $node,
    );
    $form['gid'] = array(
        '#type' => 'value',
        '#value' => isset($graphs['data'][0]['gid']) ? $graphs['data'][0]['gid'] : NULL,
    );
    $form['graph_name'] = array(
        '#title' => t("Administrative title of the Graph"),
        '#type' => 'textfield',
        '#size' => 24,
        '#maxlength' => 500,
        '#default_value' => isset($graphs['data'][0]['name']) ? $graphs['data'][0]['name'] : ''
    );

    // Add the template fieldset.
    $form['data'] = array(
        '#type' => 'fieldset',
        '#title' => t('Graph Configurations'),
        '#collapsible' => TRUE,
        '#description' => t('Graph Configuration for the Webform'),
        '#weight' => 15,
        '#tree' => FALSE,
        '#attributes' => array('id' => 'webform-template-fieldset'),
    );

    $form['data']['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Graph Title'),
        '#description' => t('Add the title of the graph'),
        '#default_value' => isset($graphs['data'][0]['title']) ? $graphs['data'][0]['title'] : '',
    );

    $form['data']['config'] = array(
        '#type' => 'textarea',
        '#title' => t('Raphael Configuration'),
        '#description' => t('Add Raphael Configuration Data for the Graph'),
        '#default_value' => isset($graphs['data'][0]['config']) ? $graphs['data'][0]['config'] : '',
        '#rows' => 10,
    );

    // Add the submit button.
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save Graph'),
        '#weight' => 20,
    );

    $form['#validate'] = array('webform_graphs_graph_edit_form_validate');

    return $form;
}

/**
 * Theme the Webform mail settings section of the node form.
 */
/*function theme_webform_graphs_graphs_edit_form($variables) {
  $form = $variables['form'];
  return drupal_render_children($form, $children);
}
*/
/**
 * Validate handler for webform_email_edit_form() and webform_emails_form().
 */
function webform_graphs_graph_name_validate($form, &$form_state) {
    $graph = trim($form_state['values']['graph_name']);
    if (empty($graph)) {
        form_set_error('graph_name', t('When adding a Graph, Graph Name field is required.'));
    }
}

/**
 * Validate handler for webform_email_edit_form().
 */
function webform_graphs_graph_edit_form_validate($form, &$form_state) {
    $config = trim($form_state['values']['config']);
    if (empty($config)) {
        form_set_error('config', t('When adding a Graph, Graph Configuration field is required.'));
    }
}

/**
 * Submit handler for webform_email_edit_form().
 */
function webform_graphs_graph_edit_form_submit($form, &$form_state) {
    // Merge the e-mail, name, address, and subject options into single values.
    $graphs = array(
        'nid' => $form_state['values']['node']->nid,
    );

    $graphs['data'][] = array(
        'gid' => $form_state['values']['gid'],
        'name' => $form_state['values']['graph_name'],
        'title' => $form_state['values']['title'],
        'config' => $form_state['values']['config'],
    );

    if (empty($form_state['values']['gid'])) {
        drupal_set_message(t('Graph configuration added.'));
        $form_state['values']['gid'] = webform_graphs_graph_insert($graphs);
    }
    else {
        webform_graphs_graph_update($graphs);
        drupal_set_message(t('Graph configuration updated.'));
    }
    $form_state['redirect'] = array('node/' . $form_state['values']['node']->nid . '/webform/graphs');
}

/**
 * Form for deleting an e-mail setting.
 */
function webform_graphs_graph_delete_form($form, $form_state, $node, $graphs) {
    $graph = $graphs['data'][0];

    $form['nid'] = array(
        '#type' => 'value',
        '#value' => $node->nid,
    );
    $form['graphs'] = array(
        '#type' => 'value',
        '#value' => $graphs,
    );

    $question = t('Delete Graph Configuration?');
    $description = t('This will immediately delete the Graph "@graph".', array('@graph' => $graph['name']));

    return confirm_form($form, $question, 'node/' . $node->nid . '/webform/graphs', $description, t('Delete'));
}

/**
 * Submit handler for webform_component_delete_form().
 */
function webform_graphs_graph_delete_form_submit($form, &$form_state) {
    drupal_set_message(t('Graph Configuration deleted.'));
    webform_graphs_graph_delete($form_state['values']['graphs']['data'][0]['gid']);
    $form_state['redirect'] = 'node/' . $form_state['values']['nid'] . '/webform/graphs';
}

function webform_graphs_graph_insert($graphs) {
    $graph = $graphs['data'][0];

    return db_insert('webform_graphs')
            ->fields(array('gid', 'nid', 'name', 'title', 'config'))
            ->values(array(
        'gid' => $graph['gid'],
        'nid' => $graphs['nid'],
        'name' => $graph['name'],
        'title' => $graph['title'],
        'config' => serialize($graph['config']),
    ))
            ->execute();
}

function webform_graphs_graph_update($graphs) {
    $graph = $graphs['data'][0];
    db_update('webform_graphs')
            ->fields(array(
        'name' => $graph['name'],
        'title' => $graph['title'],
        'config' => serialize($graph['config']),
    ))
            ->condition('gid', $graph['gid'])
            ->execute();
}

function webform_graphs_graph_delete($gid, $nid = NULL) {
    if ($nid != NULL) {
        db_delete('webform_graphs')
                ->condition('nid', $nid)
                ->execute();
    } else {
        db_delete('webform_graphs')
                ->condition('gid', $gid)
                ->execute();
    }
}

function webform_graphs_prepare_data_array($node, $submission) {
    $components = $node->webform['components'];
    $submission = $submission->data;
    $result = array();
    foreach ($submission as $id => $data) {
        $component = $components[$id];
        $component['value'] = $data['value'];
        $key = $component['form_key'];
        if (isset($result[$key]) and !is_array($result[$key])) {
            $result[$key] = array($result[$key], $component);
        } else {
            $result[$key] = $component;
        }
    }
    return $result;
}